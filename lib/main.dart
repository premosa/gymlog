import 'package:flutter/material.dart';

import './pages/MainPage.dart';

//TODO: remove this
import './pages/createPlan/CreatePlanMainPage.dart';
import './pages/createPlan/SelectExercisesPage.dart';

void main(){
  runApp(new MaterialApp(
    title: "Gym Log",
    home: new MainPage(),
  ));
}