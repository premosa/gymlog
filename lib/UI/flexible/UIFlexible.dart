import 'package:flutter/material.dart';

class UIFlexible extends StatelessWidget {
  
  final Color _backgroundColor;
  final IconData _iconData;

  final String _header;
  final String _description;

  VoidCallback _callbackFunction;

  UIFlexible(this._backgroundColor,this._iconData, this._header, this._description, this._callbackFunction);

  @override
  Widget build(BuildContext context) {
    return new Material(
        color: _backgroundColor,
        child: new InkWell(
          onTap: () => _callbackFunction(),
          child: new Container(
            padding: new EdgeInsets.only(top: 10.0, bottom: 10.0),
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Container(
                    alignment: new Alignment(0.0, -1.0),
                    padding: new EdgeInsets.only(left: 30.0, right: 30.0),
                    child: new Container(
                      decoration: new BoxDecoration(
                          border: new Border.all(
                              width: 2.0, color: Colors.black54)),
                      child: new Icon(
                        _iconData,
                        size: 40.0,
                        color: Colors.black54,
                      ),
                    )),
                new Flexible(
                  fit: FlexFit.loose,
                  child: new Container(
                    child: new Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Container(
                          alignment: new Alignment(-1.0, 0.0),
                          child: new Text(_header,
                              style: new TextStyle(
                                  color: Colors.black54,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold)),
                        ),
                        new Flexible(
                          fit: FlexFit.loose,
                          child: new Container(
                            padding: new EdgeInsets.only(top: 10.0),
                            alignment: new Alignment(-1.0, -1.0),
                            child: new Text(
                              _description,
                              style: new TextStyle(
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
