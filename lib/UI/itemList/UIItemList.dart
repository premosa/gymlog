import 'package:flutter/material.dart';

class UIItemList extends StatelessWidget {
  var _titles = [];
  var _descriptions = [];
  var _icons = [];
  var _callbackFunctions = [];
  Animation _animation;

  final Color _dataColor = Colors.black54;
  final double _itemListHeight;

  //WARNING: _titles and _descriptions must be of same length !!
  UIItemList(this._titles, this._descriptions, this._icons,this._callbackFunctions,this._itemListHeight,this._animation);

  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.white,
      child: handleItemList(),
    );
  }

  handleItemList() {
    List<Widget> itemsList = new List<Widget>();
    int i;
    //itemsList.add(createItemSpacing());
    for (i = 0; i < _titles.length; i++) {
      itemsList.add(createListItem(_titles[i], _descriptions[i], _icons[i], _callbackFunctions[i]));
      //itemsList.add(createItemSpacing());
    }
    itemsList.add(createItemSpacing());
    return new Column(children: itemsList);
  }

  createListItem(String title, String description, IconData icon, VoidCallback callbackFunction) {
    return new Material(
        color: Colors.white,
        child: new InkWell(
          onTap: () => callbackFunction(),
          child: new Container(
            height: _animation.value * _itemListHeight,
            padding: new EdgeInsets.symmetric(vertical: _animation.value *  10.0, horizontal: 15.0),
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Container(
                  padding: new EdgeInsets.only(left: 10.0, right: 20.0),
                  alignment: new Alignment(0.0, 0.0),
                  child: new Icon(
                    icon,
                    size: _animation.value * 25.0,
                    color: _dataColor,
                  ),
                ),
                new Flexible(
                  fit: FlexFit.loose,
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Container(
                        alignment: new Alignment(-1.0, 0.0),
                        child: new Text(title,
                            style: new TextStyle(
                                fontSize: _animation.value * 16.0,
                                fontWeight: FontWeight.bold,
                                color: _dataColor)),
                      ),
                      new Flexible(
                        fit: FlexFit.loose,
                        child: new Container(
                          alignment: new Alignment(-1.0, 0.0),
                          child: new Text(description,
                              style: new TextStyle(
                                  fontSize: _animation.isCompleted ? 12.0 : 0.0,
                                  color: _dataColor)),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }

  createItemSpacing() {
    return new Flexible(fit: FlexFit.loose, child: new Container());
  }
}
