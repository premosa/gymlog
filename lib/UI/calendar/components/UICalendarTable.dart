import 'package:flutter/material.dart';

class UICalendarTable extends StatefulWidget {

  // variables
  Animation _calendarAnimation;

  // constructor
  UICalendarTable(this._calendarAnimation);

  @override
  State createState() => new _StateUICalendarTable();
}

class _StateUICalendarTable extends State<UICalendarTable> {

  //TODO: make this dynamic, based on each month of the year
  var calendarData = [
    "  ","M ","T ","W ","T ","F ","S ","S ",
    "> ","  ","  ","1 ","2 ","3 ","4 ","5 ",
    "> ","6 ","7 ","8 ","9 ","10","11","12",
    "> ","13","14","15","16","17","18","19",
    "> ","20","21","22","23","24","25","26",
    "> ","27","28","29","30","31","  ","  ",
  ];

    /*var calendarData = [
    "X1","X2","X3","X4","XX","XX","XX","XX",
    "X2","X3","X4","X5","XX","XX","XX","XX",
    "X3","X4","X5","X6","XX","XX","XX","XX",
    "  ","X5","X6","X7","XX","XX","XX","XX",
    "X5","X6","X7","X8","XX","XX","XX","XX",
    "X6","X7","X8","X9","XX","XX","XX","XX",
    ];*/

  int counter = 0;

  @override
  Widget build(BuildContext context) {
    counter = 0;
    Material newMaterial = new Material(
      color: Colors.transparent,
      child: new Container(
        padding: new EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            createCalendarRow(),
            createCalendarRow(),
            createCalendarRow(),
            createCalendarRow(),
            createCalendarRow(),
            createCalendarRow(),
          ],
        ),
      ),
    );
    counter = 0; //reset counter
    return newMaterial;
  }

  createCalendarRow() {
    Flexible newRow = new Flexible(
        fit: FlexFit.loose,
        child: new Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            createCalendarDay(calendarData[counter]),
            spaceDays(),
            createCalendarDay(calendarData[counter+1]),
            spaceDays(),
            createCalendarDay(calendarData[counter+2]),
            spaceDays(),
            createCalendarDay(calendarData[counter+3]),
            spaceDays(),
            createCalendarDay(calendarData[counter+4]),
            spaceDays(),
            createCalendarDay(calendarData[counter+5]),
            spaceDays(),
            createCalendarDay(calendarData[counter+6]),
            spaceDays(),
            createCalendarDay(calendarData[counter+7]),
          ],
        ));
        counter = counter+8;
        return newRow;
  }

  spaceDays() {
    return new Flexible(
      fit: FlexFit.loose,
      child: new Container(),
    );
  }

  createCalendarDay(textValue) {
    return new Container(
      height: 20.0,
      width: 30.0,
       //decoration: new BoxDecoration(border: new Border.all( width: 2.0, color: Colors.black)),
      padding: new EdgeInsets.all(5.0),
      alignment: new Alignment(0.0, 0.0),
      child: new Text(
        textValue,
        //textAlign: TextAlign.center,
        style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0 * widget._calendarAnimation.value),
      ),
    );
  }
}
