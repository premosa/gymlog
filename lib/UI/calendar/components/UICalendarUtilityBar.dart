import 'package:flutter/material.dart';

//UI
import '../../buttons/UITextIconButton.dart';

// color theme
import '../../blueTheme.dart';

class UICalendarUtilityBar extends StatelessWidget{
  
  Animation _calendarUtilityAnimation;
  double _calendarUtilityBarHeight;

  UICalendarUtilityBar(this._calendarUtilityBarHeight,this._calendarUtilityAnimation);

  @override
  Widget build(BuildContext context){
    return new Material(
      color: Colors.transparent,
      child:           new Container(
            padding: new EdgeInsets.only(
                top: _calendarUtilityAnimation.value * 5.0,
                bottom: _calendarUtilityAnimation.value * 5.0),
            height: _calendarUtilityAnimation.value *
                _calendarUtilityBarHeight,
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Flexible(
                  fit: FlexFit.loose,
                  child: new Container(),
                ),
                new Container(
                    padding: new EdgeInsets.only(left: 10.0, right: 10.0),
                    child: new UITextIconButton("August", Icons.arrow_drop_down,
                        20.0 * _calendarUtilityAnimation.value, ColorTheme.textSecondary)),
                new Container(
                    padding: new EdgeInsets.only(left: 10.0, right: 10.0),
                    child: new UITextIconButton("2018", Icons.arrow_drop_down,
                        20.0 * _calendarUtilityAnimation.value, ColorTheme.textSecondary))
              ],
            ),
          ),
    );
  }
}