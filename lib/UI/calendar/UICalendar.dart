import 'package:flutter/material.dart';

//UI components
import './components/UICalendarUtilityBar.dart';
import './components/UICalendarTable.dart';

//color theme
import '../blueTheme.dart';

class UICalendar extends StatefulWidget {
  State createState() => new _UICalendarState();

  //variables
  final double _calendarUtilityBarHeight;
  Animation _calendarAnimation;

  //constructor
  UICalendar(this._calendarUtilityBarHeight, this._calendarAnimation);
}

class _UICalendarState extends State<UICalendar>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.transparent,
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new UICalendarUtilityBar(widget._calendarUtilityBarHeight,
              widget._calendarAnimation),
          new Flexible(
              fit: FlexFit.loose,
              child: new Container(
                child: new UICalendarTable(widget._calendarAnimation),
              ))
        ],
      ),
    );
  }
}
