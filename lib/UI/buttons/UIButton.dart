import 'package:flutter/material.dart';

class UIButton extends StatefulWidget {
  //private variables - icon data
  IconData _iconData;

  // private variables - text data
  String _textData;

  // private variables - structure
  double _size;
  VoidCallback _onTapFunction;
  bool _isIcon;

  bool _isAnimated;
  Animation _animation;

  //Constructor for Icon data
  UIButton.createIconButton(
      iconData, size, isAnimated, animation, callbackFunction) {
    _iconData = iconData;
    _size = size;
    _onTapFunction = callbackFunction;
    _isIcon = true;
    _animation = animation;
    _isAnimated = isAnimated;
  }

  //Constructor for text data
  UIButton.createTextButton(
      textData, size, isAnimated, animation, callbackFunction) {
    _textData = textData;
    _size = size;
    _onTapFunction = callbackFunction;
    _isIcon = false;
    _animation = animation;
    _isAnimated = isAnimated;
  }

  @override
  State createState() => new _StateUIButton();
}

class _StateUIButton extends State<UIButton>{

  //build function
  @override
  Widget build(BuildContext build) {
    return new Material(
        color: Colors.transparent,
        child: new InkWell(
            splashColor: Colors.white,
            onTap: () => handleTap(),
            child: new Column(
              children: <Widget>[
                new Container(
                  child: determineChild(),
                ),
                handleAnimationContainer()
              ],
            )));
  }

  handleTap() {
    widget._onTapFunction();
  }

  determineChild() {
    if (widget._isIcon) {
      return new Icon(
        widget._iconData,
        size: widget._size,
        color: Colors.white,
      );
    } else {
      return new Text(
        widget._textData,
        style: new TextStyle(
            color: Colors.white,
            fontSize: widget._size,
            fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      );
    }
  }

  handleAnimationContainer() {
    if (widget._isAnimated) {
      return new Container(
        height: 8.0,
        color: Colors.transparent,
        child: new Column(
          verticalDirection: VerticalDirection.up,
          children: <Widget>[
            new Container(
              height: widget._animation.value * 4.0,
              color: Colors.white,
            )
          ],
        ),
      );
    } else {
      return new Container(); //empty container
    }
  }
}
