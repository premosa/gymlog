//import packages
import 'package:flutter/material.dart';

//import theme
import '../blueTheme.dart';

class UITextIconButton extends StatelessWidget {
  //private variables
  IconData _icon;
  String _textContent;
  Color _contentColor;
  double _height;

  UITextIconButton(this._textContent, this._icon,this._height,this._contentColor);

  @override
  Widget build(BuildContext contex) {
    return new Material(
        color: Colors.transparent,
        child: new InkWell(
            onTap: () => print('Tapped inkwell'),
            child: new Container(
              padding: new EdgeInsets.all(3.0),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Container(
                    padding: new EdgeInsets.only(left: 5.0),
                  ),
                  new Flexible(
                    fit: FlexFit.loose,
                    child: new Container(
                      child: new Text(
                        _textContent,
                        style: new TextStyle(
                            fontSize: _height,
                            fontWeight: FontWeight.bold,
                            color: _contentColor),
                      ),
                    ),
                  ),
                  new Container(
                    child: new Icon(
                      _icon,
                      size: _height,
                      color: _contentColor,
                    ),
                  )
                ],
              ),
            )));
  }
}
