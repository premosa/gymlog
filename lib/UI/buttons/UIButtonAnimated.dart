/*
import 'package:flutter/material.dart';

class UIButtonAnimated extends StatefulWidget {
  
  //private variables - icon data
  IconData _iconData;

  // private variables - text data
  String _textData;

  // private variables - structure
  double _size;
  VoidCallback _onTapfunction;
  bool _isIcon;

  UIButtonAnimated.createAnimatedIconButton(iconData, size, onTapFunction){
    _isIcon = true;

    _size = size;
    _iconData = iconData;
    _onTapfunction = onTapFunction;
  }

  createState() => new _StateUIButtonAnimated();
}

class _StateUIButtonAnimated extends State<UIButtonAnimated> {
  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.transparent,
      child: new InkWell(
          splashColor: Colors.white,
          onTap: () => widget
              ._onTapFunction(), //TODO: make this a void callback that is passed as an arguement -- what button does
          child: new Container(
            decoration: new BoxDecoration(
                border: new Border.all(width: 4.0, color: Colors.white)),
            padding: new EdgeInsets.all(2.0),
            child: determineChild(),
          )),
    );
  }

  determineChild() {
    if (_isIcon) {
      return new Icon(
        _iconData,
        size: _size,
        color: Colors.white,
      );
    } else {
      return new Text(
        _textData,
        style: new TextStyle(
            color: Colors.white, fontSize: _size, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      );
    }
  }
}
*/