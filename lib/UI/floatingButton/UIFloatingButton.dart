import 'package:flutter/material.dart';

// Color theme
import '../blueTheme.dart';

class UIFloatingButton extends StatelessWidget {
  
  final IconData _buttonIcon;
  final VoidCallback _onTapCallback;

  UIFloatingButton(this._buttonIcon, this._onTapCallback);

  @override
  Widget build(BuildContext context) {
    return toggle();
  }

  Widget toggle() {
    return FloatingActionButton(
      backgroundColor: ColorTheme.themePrimary,
      onPressed: () => handleButtonTap(),
      shape: const RoundedRectangleBorder(),
      elevation: 0.0,
      tooltip: 'Add New Exercise',
      child: new Icon(_buttonIcon,
          size: 38.0),
    );
  }

  void handleButtonTap(){
    print("Tapped Floting Button"); //TODO: remove this print statement
    _onTapCallback();
  }
}
