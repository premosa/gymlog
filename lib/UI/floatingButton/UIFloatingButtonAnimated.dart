import 'package:flutter/material.dart';

// Color theme
import '../blueTheme.dart';

class UIFloatingButtonAnimated extends StatefulWidget {

  final IconData _defaultIcon;
  final IconData _altenateIcon;

  UIFloatingButtonAnimated(this._defaultIcon, this._altenateIcon);

  @override
  _StateUIFloatingButtonAnimated createState() => _StateUIFloatingButtonAnimated();
}

class _StateUIFloatingButtonAnimated extends State<UIFloatingButtonAnimated>
    with SingleTickerProviderStateMixin {
  bool isOpened = false;
  AnimationController _animationController;
  Animation<Color> _animateColor;
  Animation<double> _animateIcon;
  Curve _curve = Curves.easeOut;

  @override
  initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500))
          ..addListener(() {
            setState(() {});
          });
    _animateIcon =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _animateColor = ColorTween(
      begin: ColorTheme.themePrimary,
      end: Colors.lightBlue,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.00,
        1.00,
        curve: _curve,
      ),
    ));
    super.initState();
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  animate() {
    if (!isOpened) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
    isOpened = !isOpened;
  }

  Widget toggle() {
    return FloatingActionButton(
      backgroundColor: _animateColor.value,
      onPressed: animate,
       shape: const RoundedRectangleBorder(),
       elevation: 0.0,
      tooltip: 'Add New Exercise',
      child: new Icon(
         isOpened? widget._altenateIcon : widget._defaultIcon,
         size: 38.0
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return toggle();
  }
}