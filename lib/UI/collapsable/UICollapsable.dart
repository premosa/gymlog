import 'package:flutter/material.dart';

import '../blueTheme.dart';

class UICollapsable extends StatefulWidget {
  double _height;
  double _borderThickness;
  Animation _parentAnimation;
  Widget _childWidget;

  UICollapsable(this._height, this._borderThickness, this._parentAnimation,
      this._childWidget);

  @override
  State createState() => new _StateUICollapsable();
}

class _StateUICollapsable extends State<UICollapsable> {
  @override
  void initState() {
    super.initState();
    //prepare animation
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
        color: Colors.white,
        child: new Column(
          children: <Widget>[
            new Container(
              height: widget._borderThickness,
              color: ColorTheme.themePrimary,
            ),
            new Container(
              height: widget._parentAnimation.value * widget._height,
              child: widget._childWidget,
            ),
          ],
        ));
  }
}
