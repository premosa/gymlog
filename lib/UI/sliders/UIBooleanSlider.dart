import 'package:flutter/material.dart';

class UIBooleanSlider extends StatefulWidget {
  // private variables
  double _size;
  IconData _firstIconData;
  IconData _secondIconData;

  VoidCallback _onTapFunction;

  UIBooleanSlider(this._firstIconData, this._secondIconData, this._size,
      this._onTapFunction);

  @override
  State createState() => new StateUIBooleanSlider();
}

class StateUIBooleanSlider extends State<UIBooleanSlider>
    with TickerProviderStateMixin {
  Animation _firstIconAnimation;
  AnimationController _firstIconAnimationController;
  Animation _secondIconAnimation;
  AnimationController _secondIconAnimationController;

  bool _isFirstActive = true;
  IconData _currentIconData;

  @override
  void initState() {
    super.initState();

    int animationDuration = 200; // in miliseconds
    Curve animationCurve = Curves.linear;

    //prepare firstIcon Animation
    _firstIconAnimationController = new AnimationController(
        duration: new Duration(milliseconds: animationDuration), vsync: this);
    _firstIconAnimation = new CurvedAnimation(
        parent: _firstIconAnimationController, curve: animationCurve);
    _firstIconAnimation.addListener(() => this.setState(() => {}));
    _firstIconAnimationController.reset();
    _firstIconAnimationController.forward();

    //prepare firstIcon Animation
    _secondIconAnimationController = new AnimationController(
        duration: new Duration(milliseconds: animationDuration), vsync: this);
    _secondIconAnimation = new CurvedAnimation(
        parent: _secondIconAnimationController, curve: animationCurve);
    _secondIconAnimation.addListener(() => this.setState(() => {}));
    _secondIconAnimationController.reset();

    //set startIconData state - set to firstIconData
    _currentIconData = widget._firstIconData;
  }

  @override
  void dispose() {
    //dispose ob both animation controllers
    _firstIconAnimationController.dispose();
    _secondIconAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.transparent,
      child: new InkWell(
          onTap: () => handleTap(),
          splashColor: Colors.white,
          child: new Container(
            child: new InkWell(
              child: new Row(
                children: <Widget>[
                  new Expanded(
                      child: new Container(
                    padding: new EdgeInsets.all(1.0),
                    child: new Container(
                      child: prepareIcon(),
                    ),
                  )),
                ],
              ),
            ),
          )),
    );
  }

  handleTap() {
    _isFirstActive = !_isFirstActive;
    //print("isFirstActive state: " + _isFirstActive.toString());
    handleAnimation();
    //handle widget function
    widget._onTapFunction();
  }

  handleAnimation() {
    if (_isFirstActive) {
      //first, shrink firstAnimationController
      _currentIconData = widget._firstIconData;
      _secondIconAnimationController
          .reverse()
          .then((Null) => _firstIconAnimationController.reset())
          .then((Null) => _firstIconAnimationController.forward());
    } else {
      _currentIconData = widget._secondIconData;
      _firstIconAnimationController
          .reverse()
          .then((Null) => _secondIconAnimationController.reset())
          .then((Null) => _secondIconAnimationController.forward());
    }
    //prepareIcon();
  }

  prepareIcon() {
    if (_isFirstActive) {
      return new Icon(
        _currentIconData,
        //size: widget._size,
        size: widget._size * _firstIconAnimation.value,
        color: Colors.white,
      );
    } else {
      return new Icon(
        _currentIconData,
        //size: widget._size,
        size: widget._size * _secondIconAnimation.value,
        color: Colors.white,
      );
    }
  }
}
