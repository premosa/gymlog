import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

// utilities
import './utilities/UISlideElementData.dart';

// color theme
import '../blueTheme.dart';

typedef void SelectedElementCallback(Key selectedElementKey);

class UITableElement extends StatefulWidget {

  final int slideDuration;

  final Key _elementKey;
  Key _selectedKey;

  Widget _parent;
  final String _title;
  final String _abbreviation;
  final int _depthlevel;
  String _description = "No Description Given";
  bool _hasDescription = false;
  List<UISlideElementData> _leftElements = [];
  List<UISlideElementData> _rightElements = [];

  UITableElement(this._elementKey, this._title, this._abbreviation, this._leftElements, this._rightElements,
      this._depthlevel, this.slideDuration, this._selectedKey);

  Key getElementKey() {
    return _elementKey;
  }

  void setSelectedKey(Key newSelectedKey) {
    _selectedKey = newSelectedKey;
  }

  int getDepthLevel() {
    return _depthlevel;
  }

  Widget getParent() {
    return _parent;
  }

  void setParent(Widget newParent) {
    _parent = newParent;
  }

  //State functions
  void stateUpdateSelection(Key newKey) {
    stateUpdateSelection(newKey);
  }

  _UITableElementState _state = new _UITableElementState();

  @override
  createState() => _state;
}

class _UITableElementState extends State<UITableElement> {
  @override
  Widget build(BuildContext context) {
    return new Slidable(
      delegate: new SlidableDrawerDelegate(),
      actionExtentRatio: 0.25,
       movementDuration: new Duration( milliseconds: widget.slideDuration),
      child: new Container(
        color: Colors.white,
        child: new ListTile(
          leading: new CircleAvatar(
            backgroundColor: Colors.indigoAccent,
            child: new Text(widget._abbreviation),
            foregroundColor: Colors.white,
          ),
          title: new Text(widget._title),
          subtitle: new Text(widget._description, style: determineTextStyle()),
        ),
      ),
      actions: handleActions(),
      secondaryActions: handleSecondaryActions(),
    );
  }

  TextStyle determineTextStyle() {
    if (widget._hasDescription) {
      return new TextStyle(color: ColorTheme.textPrimary);
    } else {
      return new TextStyle(color: ColorTheme.textSecondary);
    }
  }

  List<Widget> handleActions() {
    List<Widget> leftElements = new List<Widget>();
    int i;
    for (i = 0; i < widget._leftElements.length; i++) {
      leftElements.add(new IconSlideAction(
          caption: widget._leftElements[i].caption,
          icon: widget._leftElements[i].icon,
          color: widget._leftElements[i].color,
          onTap: widget._leftElements[i].callbackFunction));
    }
    return leftElements;
  }

  List<Widget> handleSecondaryActions() {
    List<Widget> rightElements = new List<Widget>();
    int i;
    for (i = 0; i < widget._rightElements.length; i++) {
      rightElements.add(new IconSlideAction(
          caption: widget._rightElements[i].caption,
          icon: widget._rightElements[i].icon,
          color: widget._rightElements[i].color,
          onTap: widget._rightElements[i].callbackFunction));
    }
    return rightElements;
  }

  void updateSelectionState(Key newKey) {
    if (mounted) {
      setState(() {
        widget._selectedKey = newKey;
      });
    }
  }
}
