import 'package:flutter/material.dart';

class UISlideElementData
{
  final IconData icon;
  final String caption;
  final Color color;
  VoidCallback callbackFunction;

  UISlideElementData(this.icon,this.caption,this.color,this.callbackFunction);
}