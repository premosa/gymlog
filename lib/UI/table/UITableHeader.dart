import 'package:flutter/material.dart';

// color theme
import '../blueTheme.dart';

typedef void SelectedElementCallback(Key selectedElementKey);

class UITableHeader extends StatelessWidget {
  final String _title;
  final double _iconContainerWidth;

  UITableHeader(
      this._title, this._iconContainerWidth);

  final double _fontSize = 20.0;
  final double _topPadding = 10.0;

  @override
  Widget build(BuildContext context) {
    return new Material(
        color: ColorTheme.mainBackgroundColor,
        child: new Container(
          padding: new EdgeInsets.only(
              left: 10.0, top: _topPadding, bottom: _topPadding),
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Flexible(
                  fit: FlexFit.loose,
                  child: new Row(
                    children: <Widget>[
                      new Flexible(
                          fit: FlexFit.loose,
                          child: new Material(
                              color: Colors.transparent,
                              child: new Row(
                                children: determineLeftSideChildren(),
                              ))),
                      /*new Container(
                        alignment: new Alignment(1.0, 0.0),
                        child: new Row(
                          mainAxisSize: MainAxisSize.min,
                          children: determineRightSideChildren(),
                        ),
                      )*/
                    ],
                  ))
            ],
          ),
        ));
  }

  determineLeftSideChildren() {
    List<Widget> result = new List<Widget>();
    // HEADER ELEMENT
    Flexible header = new Flexible(
      fit: FlexFit.loose,
      child: new Text(
        _title,
        style: new TextStyle(
            fontSize: _fontSize,
            color: ColorTheme.themePrimary,
            fontWeight: FontWeight.bold),
      ),
    );
    result.add(header);
    return result;
  }

  /*determineRightSideChildren() {
    List<Widget> result = new List<Widget>();
    int i;
    //HEADER DATA
    for (i = 0; i < _iconCount; i++) {
      result.add(new Flexible(
        fit: FlexFit.loose,
        child: new Container(
            padding: new EdgeInsets.only(right: 5.0),
            child: Material(
              color: Colors.transparent,
              child: new Container(
                height: _iconContainerWidth,
                width: _iconContainerWidth,
                alignment: new Alignment(0.0, 0.0),
                child: new Icon(_icons[i], size: _iconContainerWidth - 4.0),
              ),
            )),
      ));
    }
    return result;
  }*/
}
