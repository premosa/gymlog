import 'package:flutter/material.dart';

//if using alternate color scheme, simply import a diffrent .dart file, but class  and variable names should be the same (just modify the values)
class ColorTheme{
  static Color themePrimary = Colors.blue;
  static Color themeSecondary = Colors.blue[800];
  static Color themeHighlight = Colors.blue[300];
  static Color themeDanger = Colors.redAccent;

  static Color textPrimary = Colors.white;
  static Color textSecondary = Colors.grey;

  static Color mainBackgroundColor = Colors.white;
}