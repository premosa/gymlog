import 'package:flutter/material.dart';

// color theme
import '../blueTheme.dart';

// defined callback function
typedef void StringCallback(String value);

class UISearchField extends StatefulWidget {
  // variables
  StringCallback _callbackFunction;

  //Construction
  UISearchField(this._callbackFunction);

  @override
  State createState() => new _UISearchFieldState();
}

class _UISearchFieldState extends State<UISearchField> {
  TextEditingController _textEditingController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Material(
        color: ColorTheme.mainBackgroundColor,
        child: new Container(
            child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Flexible(
                fit: FlexFit.loose,
                child: new Row(
                  children: <Widget>[
                    new Container(
                        /*child: new Icon(
                            Icons.keyboard_arrow_right,
                            size: 25.0,
                            color: ColorTheme.themePrimary,
                          ),*/
                        ),
                    new Flexible(
                      fit: FlexFit.loose,
                      child: new TextField(
                        decoration: new InputDecoration(
                          fillColor: Colors.transparent,
                            labelStyle:
                                new TextStyle(color: ColorTheme.textSecondary),
                                filled: true,
                            border: new UnderlineInputBorder(
                                borderSide: new BorderSide(color: Colors.red))),
                        controller: _textEditingController,
                        onSubmitted: (result) => onSearchFieldSubmit(result),
                        style: new TextStyle(
                            color: ColorTheme.themePrimary, fontSize: 16.0),
                             cursorColor: ColorTheme.themePrimary,
                      ),
                    )
                  ],
                ))
          ],
        )));
  }

  void onSearchFieldSubmit(submitResult) {
    widget._callbackFunction(submitResult);
    _textEditingController.clear();
  }
}
