import 'package:flutter/material.dart';

class UIOverlayDeleteElement extends StatefulWidget {
  @override
  State createState() => new _StateUIOverlayDeleteElement();
}

class _StateUIOverlayDeleteElement extends State<UIOverlayDeleteElement> {
  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.black26,
      child: new InkWell(
          onTap: () => print("Tapped UIOverlayDeleteElement"),
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Flexible(
                fit: FlexFit.loose,
                child: new Container(),
              ),
              new Flexible(
                fit: FlexFit.tight,
                  child: new Column(
                children: <Widget>[
                  new Container(
                    child: new Icon(Icons.delete),
                  ),
                  new Text("Delete element")
                ],
              ),
              ),
              new Flexible(
                fit: FlexFit.loose,
                 child: new Container(),
              )
            ],
          )),
    );
  }
}
