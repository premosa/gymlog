import 'package:flutter/material.dart';
import 'dart:async';

//import color pallete
import '../../blueTheme.dart';

// UI Elements
import '../../buttons/UIButton.dart';
import '../../sliders/UIBooleanSlider.dart';
import '../../collapsable/UICollapsable.dart';
import '../../calendar/UICalendar.dart';
import '../../itemList/UIItemList.dart';

//pages
import '../../../pages/createPlan/CreatePlanMainPage.dart';

class UIBottomNavigationMainPage extends StatefulWidget {
  @override
  State createState() => new _UIBottomNavigationMainPageState();
}

class _UIBottomNavigationMainPageState extends State<UIBottomNavigationMainPage>
    with TickerProviderStateMixin {
  //Calendar variables
  double _calendarBarHeight = 210.0;
  double _calendarUtilityBarHeight = 50.0;

  final int _settingsElementCount = 3;
  final double _settingsElementHeight = 70.0;

  final int _animationDuration = 400; // in miliseconds

  Animation _calendarAnimation;
  AnimationController _calendarAnimationController;
  bool _isCalendarActive = false;

  Animation _settingsAnimation;
  AnimationController _settingsAnimationController;
  bool _isSettingsActive = false;

  int expandedState = 0;
  /*
  expanded state:
  = 0 ... no state expanded
  = 1 ... TOC button expanded
  = 2 ... Calendar expanded
  */

  @override
  void dispose() {
    _calendarAnimationController.dispose();
    _settingsAnimationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    //prepare calendar animations
    _calendarAnimationController = new AnimationController(
        duration: new Duration(milliseconds: _animationDuration), vsync: this);
    _calendarAnimation = new CurvedAnimation(
        parent: _calendarAnimationController, curve: Curves.ease);
    _calendarAnimation.addListener(() => this.setState(() {}));
    _calendarAnimationController.reset();

    //prepare settings animations
    _settingsAnimationController = new AnimationController(
        duration: new Duration(milliseconds: _animationDuration), vsync: this);
    _settingsAnimation = new CurvedAnimation(
        parent: _settingsAnimationController, curve: Curves.ease);
    _settingsAnimation.addListener(() => this.setState(() {}));
    _settingsAnimationController.reset();
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.transparent,
      child: new Column(
        children: <Widget>[
          new Container(
              child: new UICollapsable(
                  _calendarBarHeight,
                  10.0,
                  _calendarAnimation,
                  new UICalendar(
                      _calendarUtilityBarHeight, _calendarAnimation))),
          new Container(
            child: new UICollapsable(
                _settingsElementHeight * _settingsElementCount,
                10.0,
                _settingsAnimation,
                new UIItemList([
                  "Create New Plan",
                  "Modify Existing Plans",
                  "Select an Active Plan"
                ], [
                  "Prepare a set of exercises and sort them in a weekly routine",
                  "Select an existing plan and modify it's properties",
                  "From list of plans, select one that will used as template for each week"
                ], [
                  Icons.create,
                  Icons.brush,
                  Icons.clear_all
                ], [
                  () {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new CreatePlanMainPage()));
                  },
                  () {/*TODO: implement this*/},
                  () {/*TODO: implement this*/}
                ], _settingsElementHeight, _settingsAnimation)),
          ),
          new Container(
            color: ColorTheme.themePrimary,
            padding: new EdgeInsets.only(
                left: 10.0, bottom: 10.0, right: 10.0, top: 8.0),
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Flexible(
                    fit: FlexFit.loose,
                    child: new Column(
                      children: <Widget>[
                        new UIButton.createIconButton(Icons.toc, 30.0, true,
                            _settingsAnimation, () => handleExpandButtons(1)),
                      ],
                    )),
                new Container(
                  padding: new EdgeInsets.only(left: 10.0),
                ),
                new Flexible(
                    fit: FlexFit.loose,
                    child: new UIButton.createIconButton(
                        Icons.calendar_today,
                        30.0,
                        true,
                        _calendarAnimation,
                        () => handleExpandButtons(2))),
                new Container(padding: new EdgeInsets.only(left: 10.0)),
                new Flexible(
                  fit: FlexFit.loose,
                  child: new UIBooleanSlider(
                      Icons.remove_red_eye,
                      Icons.mode_edit,
                      30.0,
                      () => print("Tapped on UIBooleanSlider")),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  handleExpandButtons(int tappedButton) {
    if (tappedButton == 1) {
      // tapped settings list - TOC
      if (this.expandedState == 2) {
        //close calendar view
        swapCalendarState();
        timerDelay(_animationDuration, () {
          swapSettingsState();
          this.expandedState = 1;
        });
      } else {
        swapSettingsState();
        if (this.expandedState == 0) {
          this.expandedState = 1;
        } else {
          this.expandedState = 0;
        }
      }
    } else if (tappedButton == 2) {
      // tapped Calendar settings
      if (this.expandedState == 1) {
        swapSettingsState();
        timerDelay(_animationDuration, () {
          swapCalendarState();
          this.expandedState = 2;
        });
      } else {
        swapCalendarState();
        if (this.expandedState == 0) {
          this.expandedState = 2;
        } else {
          this.expandedState = 0;
        }
      }
    }
  }

  timerDelay(int delayMiliseconds, callbackFunc) {
    new Timer(new Duration(milliseconds: delayMiliseconds), callbackFunc);
  }

  swapCalendarState() {
    //print("Tapped Calendar Button");
    if (_isCalendarActive) {
      //hide calendar
      _calendarAnimationController.reverse();
    } else {
      //show calendar
      _calendarAnimationController.forward();
    }
    this._isCalendarActive = !this._isCalendarActive;
  }

  swapSettingsState() {
    //print("Tapped TOC Butoon");
    if (_isSettingsActive) {
      // hide settings
      _settingsAnimationController.reverse();
    } else {
      // show settings
      _settingsAnimationController.forward();
    }
    this._isSettingsActive = !this._isSettingsActive;
  }
}
