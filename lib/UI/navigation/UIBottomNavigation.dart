import 'package:flutter/material.dart';

//color theme
import '../blueTheme.dart';

class UIBottomNavigation extends StatelessWidget {
  VoidCallback _backFunction;
  VoidCallback _saveFunction;

  UIBottomNavigation(this._backFunction, this._saveFunction);

  @override
  Widget build(BuildContext context) {
    return new Material(
      child: new Container(
          height: 65.0,
          color: ColorTheme.themePrimary,
          child: new Row(
            children: <Widget>[
              new Container(
                child: new Material(
                  color: Colors.transparent,
                  child: new InkWell(
                    onTap: () => _backFunction(),
                    child: new Container(
                      padding: new EdgeInsets.only(left: 30.0, right: 30.0),
                      alignment: new Alignment(0.0, 0.0),
                      child: new Icon(
                        Icons.keyboard_arrow_left,
                        size: 40.0,
                        color: ColorTheme.textPrimary,
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                width: 3.0,
                color: Colors.white,
              ),
              new Flexible(
                fit: FlexFit.loose,
                child: new Material(
                  color: Colors.transparent,
                  child: new InkWell(
                    onTap: () => _saveFunction(),
                    child: new Container(
                      alignment: new Alignment(0.0, 0.0),
                      child: new Text(
                        "Save",
                        style: new TextStyle(
                            color: ColorTheme.textPrimary,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
