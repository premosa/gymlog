import 'package:flutter/material.dart';

//color themes
import '../blueTheme.dart';

class UITopNavigation extends StatelessWidget {
  // variables
  String _headerData;

  // constructor
  UITopNavigation(this._headerData);

  @override
  Widget build(BuildContext context) {
    return new Material(
      child: new Container(
        child: new Row(
          children: <Widget>[
            new Expanded(
              child: new Container(
                color: ColorTheme.themePrimary,
                padding:
                    new EdgeInsets.only(left: 15.0, top: 30.0, bottom: 20.0),
                child: new Text(
                  _headerData,
                  style: new TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      color: ColorTheme.textPrimary),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
