import 'package:flutter/material.dart';

// color
import '../../UI/blueTheme.dart';

// UI
import '../../UI/navigation/UITopNavigation.dart';
import '../../UI/navigation/UIBottomNavigation.dart';
import '../../UI/flexible/UIFlexible.dart';

// pages
import '../createPlan/SelectExercisesPage.dart';

class CreatePlanMainPage extends StatefulWidget {
  @override
  State createState() => new _StateCreatePlanMainPage();
}

class _StateCreatePlanMainPage extends State<CreatePlanMainPage>
    with SingleTickerProviderStateMixin {
  Animation _planAnimation;
  AnimationController _planAnimationController;

  final int _animationDuration = 400; // in miliseconds
  final double _itemListHeight = 75.0;

  //variables
  final bool _isCreateExercisesDone = true;
  final bool _isSortExercisesDone = false;

  final IconData _isDoneIcon = Icons.done;
  final IconData _isMissingIcon = null;

  @override
  void initState() {
    //prepare initState
    super.initState();

    //prepare calendar animations
    _planAnimationController = new AnimationController(
        duration: new Duration(milliseconds: _animationDuration), vsync: this);
    _planAnimation = new CurvedAnimation(
        parent: _planAnimationController, curve: Curves.ease);
    _planAnimation.addListener(() => this.setState(() {}));
    _planAnimationController.reset();
    _planAnimationController.forward(); //set initial state to true
  }

  @override
  void dispose() {
    //dispose of animations
    _planAnimationController.dispose();
    //call super dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
      child: new Column(
        children: <Widget>[
          new UITopNavigation("Create New Plan"),
          new Expanded(
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Expanded(
                  child: new UIFlexible(
                      Colors.white,
                      determineIcon(0),
                      "Select Exercises",
                      "Select or create exercises and exercises groups. Use labels for better filtering later.",
                      () {
                    Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context)=> new SelectExercisesPage()));
                  }),
                ),
                new Expanded(
                  child: new UIFlexible(
                      Colors.white,
                      determineIcon(1),
                      "Sort Exercises",
                      "Sort prepared exercises into standard weekly routine. Additional daily notes can be added dynamically.",
                      () {
                    print("clicked Sort Exercises");
                  }),
                ),
              ],
            ),
          ),
          //Custom Footer
          new UIBottomNavigation((){/*TODO: implement this*/},(){/*TODO: implement this*/})
        ],
      ),
    );
  }

  determineIcon(int child) {
    var states = [_isCreateExercisesDone, _isSortExercisesDone];
    if (states[child]) {
      return _isDoneIcon;
    } else {
      return _isMissingIcon;
    }
  }
}
