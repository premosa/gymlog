import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:async';
import 'package:uuid/uuid.dart';

//UI
import '../../UI/table/UITableElement.dart';
import '../../UI/table/UITableHeader.dart';
import '../../UI/table/utilities/UISlideElementData.dart';
import '../../UI/navigation/UITopNavigation.dart';
import '../../UI/navigation/UIBottomNavigation.dart';
import '../../UI/searchfield/UISearchField.dart';
import '../../UI/floatingButton/UIFloatingButton.dart';

// color theme
import '../../UI/blueTheme.dart';

// static variables
//const int INT_SIZE = 4294967296; // (TODO: remove this)

class SelectExercisesPage extends StatefulWidget {
  @override
  State createState() => new _SelectExercisesPageState();
}

class _SelectExercisesPageState extends State<SelectExercisesPage> {
  bool _isElementSelected = false;
  Key _selectedElementKey;

  final int slideDuration = 200; //ms
  Timer _animationDeletionDelay;

  List<Widget> _tableElements = [];

  @override
  void initState() {
    super.initState();

    _tableElements.add(new UITableHeader("Exercise List", 30.0));
    _tableElements.add(new UISearchField((value) {
      addElementToList(value);
    }));
  }

  void addElementToList(String titleValue) {
    if (titleValue.length < 1) {
      print(
          "Expected Error: Input titleValue is too short. It must be atleast 1 character long");
      return;
    }

    Key newKey = new ObjectKey(new Uuid());
    if (isElementInList(newKey)) {
      print("Unexpected Error: Element UUID already exists in list...");
      return;
    }

    String newTitle = titleValue;
    int index = max(1, _tableElements.length - 1);

    UISlideElementData elementEdit = new UISlideElementData(Icons.description,
        "Edit", ColorTheme.themePrimary, () => print("Tapped Edit")); //TODO
    UISlideElementData elementLabels = new UISlideElementData(
        Icons.label,
        "Labels",
        ColorTheme.themeSecondary,
        () => print("Tapped Labels")); //TODO
    UISlideElementData elementDelete = new UISlideElementData(Icons.delete,
        "Delete", ColorTheme.themeDanger, () => deleteKeyCallback(newKey));

    UITableElement newElement = new UITableElement(
        newKey,
        newTitle,
        createAbbreviation(newTitle),
        [elementEdit, elementLabels],
        [elementDelete],
        0,
        slideDuration,
        _selectedElementKey);

    _tableElements.insert(index, new Container(child: newElement, key: newKey));
  }

  String createAbbreviation(String title) {
    String result = "";
    List<String> titleSplit = title.split(" ");
    if (titleSplit.length == 1) {
      if (title.length == 1) {
        result = title.substring(0, 1);
      } else if (title.length >= 2) {
        result = title.substring(0, 2);
      }
    } else {
      result = titleSplit[0].substring(0, 1);
      result = result + titleSplit[1].substring(0, 1);
    }
    result = result.toUpperCase();
    return result;
  }

  bool isElementInList(Key newKey) {
    int i;
    for (i = 0; i < _tableElements.length; i++) {
      if (_tableElements is Container) {
        if (_tableElements.cast<Container>().elementAt(i).key == newKey) {
          return true;
        }
      }
    }
    return false;
  }

  void deleteKeyCallback(Key newKey) {
    _animationDeletionDelay =
        new Timer(new Duration(milliseconds: slideDuration), () {
      removeElementFromList(newKey);
      handleSelection(null);
    });
  }

  void setSelection(Key newSelectedKey) {
    if (_isElementSelected) {
      if (_selectedElementKey == newSelectedKey) {
        //we clicked same element again, unselect it
        _selectedElementKey = null;
        _isElementSelected = false;
      } else {
        _selectedElementKey = newSelectedKey;
      }
    } else {
      _isElementSelected = true;
      _selectedElementKey = newSelectedKey;
    }
    handleSelection(_selectedElementKey);
  }

  void handleSelection(Key selectedElementKey) {
    // for all elements, set which element is currently active
    int i;
    for (i = 0; i < _tableElements.length; i++) {
      int ind = i;
      if (_tableElements[ind] is Container) {
        UITableElement child =
            _tableElements.cast<Container>().elementAt(ind).child;
        child.stateUpdateSelection(selectedElementKey);
      }
    }
  }

  void removeElementFromList(Key uniqueKey) {
    int i;
    if (_tableElements.length == 3) {
      _tableElements.removeAt(1);
    } else {
      for (i = 1; i < _tableElements.length; i++) {
        if (_tableElements[i] is Container) {
          if (uniqueKey == _tableElements.cast<Container>().elementAt(i).key) {
            _tableElements.removeAt(i);
            break;
          }
        }
      }
    }

    this.setState(() {
      _selectedElementKey = this._selectedElementKey;
      _tableElements = _tableElements;
      _selectedElementKey = null;
      _isElementSelected = false;
    });
  }

  void moveElementsSinceIndex(int index) {
    int i;
    for (i = index; i < _tableElements.length - 1; i++) {
      Widget _tmp = _tableElements[i];
      _tableElements[i] = _tableElements[i + 1];
      _tableElements[i + 1] = _tmp;
    }
    _tableElements.removeLast();
  }

  @override
  void dispose() {
    if (_animationDeletionDelay != null) {
      _animationDeletionDelay.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Material(
        color: ColorTheme.mainBackgroundColor,
        child: new Stack(
          //fit: StackFit.expand,
          children: <Widget>[
            new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new UITopNavigation("Manage Exercises"),
                new Flexible(
                    fit: FlexFit.loose,
                    child: new Stack(
                      children: <Widget>[
                        new ListView.builder(
                          itemCount: 1,
                          itemBuilder: (BuildContext context, int ind) {
                            return new Column(children: _tableElements);
                          },
                        ),
                        new Container(
                          alignment: new Alignment(1.0, -1.0),
                          child: new UIFloatingButton(Icons.save, () {
                            print("TODO: implement this");
                          }),
                        ),
                        new Container(
                          alignment: new Alignment(0.9, 0.9),
                          child: new UIFloatingButton(Icons.save, () {
                            print("TODO: implement this");
                          }),
                        )
                      ],
                    )),
                //new UIFloatingButton(_defaultIcon, _altenateIcon)
                /*new UIBottomNavigation(() {
                  Navigator.pop(context);
                }, () {
                  print("Save clicked"); //TODO
                }),*/
              ],
            ),
          ],
        ));
  }
}
