import 'package:flutter/material.dart';

//import color pallete
import '../UI/blueTheme.dart';

// UI Elements
import '../UI/navigation/UITopNavigation.dart';
import '../UI/navigation/specific/UIBottomNavigationMainPage.dart';

class MainPage extends StatefulWidget {
  @override
  State createState() => new _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.white,
      child: new Column(
        children: <Widget>[
          new UITopNavigation("Gym Log"),
          new Expanded(
            child: new Text("Main Data ... Under construction"),
          ),
          new UIBottomNavigationMainPage()
        ],
      ),
    );
  }
}
